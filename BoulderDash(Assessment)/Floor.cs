﻿using Microsoft.Xna.Framework.Graphics;

namespace BoulderDash_Assessment_
{
    class Floor : Tile
    {

        // ------------------
        // Behaviour
        // ------------------
        public Floor(Texture2D newTexture)
            : base(newTexture)
        {
        }
        // ------------------
    }
}
