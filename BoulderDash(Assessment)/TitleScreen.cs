﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace BoulderDash_Assessment_
{
    class TitleScreen : Screen
    {
        // ------------------
        // Data
        // ------------------
        private Text gameName;
        private Text startPrompt;
        private Game1 game;

        // ------------------
        // Behaviour
        // ------------------
        public TitleScreen(Game1 newGame)
        {
            game = newGame;
        }
        // ------------------
        public void LoadContent(ContentManager content, GraphicsDevice graphics)
        {
            SpriteFont titleFont = content.Load<SpriteFont>("fonts/largeFont"); // adds the fonts
            SpriteFont smallFont = content.Load<SpriteFont>("fonts/mainFont"); // adds the font

            gameName = new Text(titleFont); //sets which font will be used 
            gameName.SetTextString("BOULDERDASH"); // sets what the text will say
            gameName.SetAlignment(Text.Alignment.CENTRE); // sets the alignment
            gameName.SetColor(Color.White); // sets the colour
            gameName.SetPosition(new Vector2(graphics.Viewport.Bounds.Width / 2, 100)); // sets the position on screen

            startPrompt = new Text(smallFont); // sets which font will be used
            startPrompt.SetTextString("[Press ENTER to Start!]"); // sets what the text will say
            startPrompt.SetAlignment(Text.Alignment.CENTRE); // sets the alligment 
            startPrompt.SetColor(Color.White); // sets the colour
            startPrompt.SetPosition(new Vector2(graphics.Viewport.Bounds.Width / 2, 200)); // sets the position on screen
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            gameName.Draw(spriteBatch);
            startPrompt.Draw(spriteBatch);
        }
        // -------------------
        public override void Update(GameTime gameTime)
        {
            //Check if the player has pressed enter

            // Get the current keyboard state
            KeyboardState keyboardState = Keyboard.GetState();

            // If the player has pressed enter.....

            if (keyboardState.IsKeyDown(Keys.Enter))
            {
                //TODO: CHANGE TO LEVEL SCREEN

                game.ChangeScreen("level");
            }
        }
        // ------------------
    }
}

