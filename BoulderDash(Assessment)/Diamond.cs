﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace BoulderDash_Assessment_
{
    class Diamond : Tile
    {
        // ------------------
        // Data
        // ------------------
        private Level ourLevel;
        private bool playerCollecting;

        private Vector2 direction;

        private float timeSinceLastMove = 0;

        private const float MOVE_COOLDOWN = 2f;


        // ------------------
        // Behaviour
        // ------------------
        public Diamond(Texture2D newTexture, Level newLevel)
            : base(newTexture)
        {
            ourLevel = newLevel;
        }

        public bool TryPush(Vector2 Direction)
        {
            // New position the boulder will be in after the push
            Vector2 newGridPos = GetTilePosition() + Direction;



            // Ask the level what is in this slot already
            Tile tileInDirection = ourLevel.GetTileAtPosition(newGridPos);


            // If the target tile is a wall, we can't move there - return false.
            if (tileInDirection != null && tileInDirection is Wall)
            {
                return false;
            }
            if (tileInDirection != null && tileInDirection is Boulder)
            {
                return false;
            }

            // If the target tile dirt, we can't move the boulder there so return false
            if (tileInDirection != null && tileInDirection is Dirt)
            {
                return false;
            }

            // If the target tile dirt, we can't move the boulder there so return false


            if (tileInDirection != null && tileInDirection is Diamond) // Prevent the diamond to get pushed by other diamonds
            {
                return false;
            }




            // Move our tile (boulder) to the new position
            return ourLevel.TryMoveTile(this, newGridPos);
        }
        // ------------------
        public bool Gravity_Sliding() // function that has adds gravity to the object as well as adds the sliding feature
        {
            Vector2 direction = new Vector2();
            direction.Y = 1f;
            Vector2 newGridPos = GetTilePosition() + direction;
            // Ask the level what is in this slot already
            Tile tileInDirection = ourLevel.GetTileAtPosition(newGridPos);

            if (tileInDirection != null && tileInDirection is Wall) // applies the gravity and sliding if on top of a wall
            {
                direction.X = 1f; // makes the diamond move to the right
                newGridPos = GetTilePosition() + direction;
                tileInDirection = ourLevel.GetTileAtPosition(newGridPos);

                if (tileInDirection != null && tileInDirection is Dirt || tileInDirection is Wall
                     || tileInDirection is Boulder || tileInDirection is Diamond)
                {
                    direction.X = -1f; // make the diamond move left
                    newGridPos = GetTilePosition() + direction;
                    tileInDirection = ourLevel.GetTileAtPosition(newGridPos);

                    if (tileInDirection != null && tileInDirection is Dirt || tileInDirection is Wall
                    || tileInDirection is Boulder || tileInDirection is Diamond)
                    {
                        direction.X = 0f; // sets the diamond to stop moving left or right
                        direction.Y = 1f; // makes the diamond fall downing coupled with the x direction creates the sliding

                        return false;
                    }
                }

            }

        
            if (tileInDirection != null && tileInDirection is Boulder) // applies the gravity and sliding if on top of a boulder
            {

                direction.X = 1f; // makes the diamond move to the right
                newGridPos = GetTilePosition() + direction; // New position the boulder will be in after the
                tileInDirection = ourLevel.GetTileAtPosition(newGridPos);// asks the level what is in the slot already

                if (tileInDirection != null && tileInDirection is Dirt || tileInDirection is Wall
                     || tileInDirection is Boulder || tileInDirection is Diamond)
                {
                    direction.X = -1f;  // make the diamond move left
                    newGridPos = GetTilePosition() + direction;
                    tileInDirection = ourLevel.GetTileAtPosition(newGridPos);

                    if (tileInDirection != null && tileInDirection is Dirt || tileInDirection is Wall
                    || tileInDirection is Boulder || tileInDirection is Diamond)
                    {
                        direction.X = 0f; // sets the diamond to stop moving left or right
                        direction.Y = 1f; // makes the diamond fall downing coupled with the x direction creates the sliding

                        return false;
                    }
                }

            }

            if (tileInDirection != null && tileInDirection is Diamond) // applies the gravity and sliding if on top of diamond
            {
                direction.X = 1f; // makes the diamond move to the right
                newGridPos = GetTilePosition() + direction;
                tileInDirection = ourLevel.GetTileAtPosition(newGridPos);

                if (tileInDirection != null && tileInDirection is Dirt || tileInDirection is Wall
                    || tileInDirection is Boulder || tileInDirection is Diamond)
                {
                    direction.X = -1f; // make the diamond move left
                    newGridPos = GetTilePosition() + direction;
                    tileInDirection = ourLevel.GetTileAtPosition(newGridPos);

                    if ((tileInDirection != null && tileInDirection is Dirt || tileInDirection is Wall
                     || tileInDirection is Boulder || tileInDirection is Diamond))
                    {
                        direction.X = 0f; // sets the diamond to stop moving left or right
                        direction.Y = 1f; // makes the diamond fall downing coupled with the x direction creates the sliding

                        return false;
                    }
                }

            }
            if (tileInDirection != null && tileInDirection is Dirt)
            {
                return false;
            }


            // Ask the level what is on the floor in this direction
            Tile floorInDirection = ourLevel.GetFloorAtPosition(newGridPos);
            return ourLevel.TryMoveTile(this, newGridPos);
        }
        // ------------------
        public override void Update(GameTime gameTime)
        {
            // Add to time since we last moved
            float frameTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            timeSinceLastMove += frameTime;

            // If there is nothing under the boulder let it fall on a timer
            if (timeSinceLastMove >= MOVE_COOLDOWN)
            {
                Gravity_Sliding(); // gravity and sliding
                timeSinceLastMove = 0;
            }


        }
        // ------------------
        
            

    }
}

