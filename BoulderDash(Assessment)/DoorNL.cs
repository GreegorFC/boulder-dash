﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BoulderDash_Assessment_
{
    class DoorNL:Tile
    {
        // ------------------
        // Data
        // ------------------

        private Level level;
        private bool isDoorOpen = false;
        private Texture2D isClosed;
        private Texture2D isOpened;

        // ------------------
        // Behaviour
        // ------------------

        public DoorNL(Texture2D newIsClosedTexture, Texture2D newIsOpenedTexture, Level newLevel)
            : base(newIsClosedTexture)
        {
            level = newLevel;
            isClosed = newIsClosedTexture;
            isOpened = newIsOpenedTexture;
        }
        // ------------------
        // ------------------
        public bool TouchDoor(Vector2 direction)
        {
            // New position the box will be in after the push
            Vector2 newGridPos = GetTilePosition() + direction;

            // Ask the level what is in this slot already
            Tile tileInDirection = level.GetTileAtPosition(newGridPos);

            // If the door is open and the player collides with it then progress to the next level
            if (tileInDirection != null && tileInDirection is Player && isDoorOpen == true)
            {
                level.Victory();
            }
            // Ask the level what is on the floor in this direction
            Tile floorInDirection = level.GetFloorAtPosition(newGridPos);

            // Move our tile (box) to the new position
            return level.TryMoveTile(this, newGridPos);
        }
        // ------------------
        public void OpenDoor()
        {
            isDoorOpen = true;
            texture = isOpened;
        }
        // ------------------
        public void CloseDoor()
        {
            isDoorOpen = false;
            texture = isClosed;
        }
       
    }
}
