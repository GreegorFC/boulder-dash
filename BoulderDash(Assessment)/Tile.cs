﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace BoulderDash_Assessment_
{
    class Tile : Sprite // tile inherits from the sprite class 
    {
        // ------------------
        // data
        // ------------------
        private Vector2 tilePosition;

        private const int TILE_SIZE = 100;


        // ------------------
        // Behaviour
        // ------------------

        public Tile(Texture2D newTexture)
            : base(newTexture)
        {
        }
        // ------------------
        public void SetTilePosition(Vector2 newTilePosition)
        {

            tilePosition = newTilePosition;
            //set our position based on tile position
            // multiply tile position by tile size 
            SetPosition(tilePosition * TILE_SIZE);

        }


        // ------------------
        public virtual void Update(GameTime gameTime)
        {
            // blank - to be implemented by child classes
        }
        // ------------------
        public Vector2 GetTilePosition()
        {
            return tilePosition;
        }
        // ------------------


    }

}

